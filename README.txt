This is a Drupal Module - Nodeorder Field
It implements a Nodeorder field for CCK - allows to set the nodeorder
for the node using autocomplete field. 
Also you can see the previous and the next nodes in order.


Copyright 2011-01 Normunds Puzo, IDYNAMIC, Ltd.

Licensed under the GNU Public License
